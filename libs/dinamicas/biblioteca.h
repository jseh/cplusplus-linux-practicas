#ifndef __BIBLIOTECA_H
#define __BIBLIOTECA_H

namespace jseh{

	int func(int, int);
	void apuntadores_const();
	void arrays();
	void punteros();
	void referencias();
	void cadenas();


	class Gato
	{
	public:
		void Maullar();
		
	};
	
}


#endif
