#include "biblioteca.h"
#include <string>
#include <iostream>
//para usar NULL
#include <cstddef>

// deber ir en las definiciones como en el encabezado .h
namespace jseh{

	int func(int a, int b){
		return a+b;
	}


	void apuntadores_const(){
		// es un apuntador a un valor entero constante
		const int* apUno = NULL;
		// es un apuntador constante, se puede cambiar el entero,  appDos no pueden apuntar a ninguna otra cosa
		int* const apDos = NULL;
		// es un apuntador constante a un entero constante, el valor al que apunta no se puede cambiar, y no pueden apuntar a ninguna otra cosa
		const int* const apTres = NULL;
	}

	void arrays(){


		// un array es un puntero constante
		// el array tiene su propia direccion y cada uno de los elementos tambien
		int ar[] = {3,5,6,8,9};

		// puntero a array
		int (*ptr)[5];
		int* pa;

		// asignar al array completo
		pa = ar;

		std::cout << "valor de elemento: " << *(pa + 1) << std::endl;
		std::cout << "direccion de elemento: " << (pa + 1) << std::endl;
		std::cout << "direccion que guarda puntero a array: " << pa << std::endl;
		std::cout << "direccion de array: " << ar << std::endl;
		// std::cout << *ptr[1] << std::endl;


		int ar2[4];
		int ar3[4][4];

		int ar4[3][4] = { 
        		            { 10, 11, 12, 13 }, 
                		    { 20, 21, 22, 23 }, 
                    		{ 30, 31, 32, 33 } 
                  		}; 


        int ar5[] = {2,4,6};

        // array de punteros
        int* arp[3];
        int* arp2D[3][2];
        int* arp3D[3][3][3];

        arp[0] = &ar5[0];
        arp[1] = &ar5[1];
        arp[2] = &ar5[2];

   		std::cout << *arp[1] << std::endl;
        
	}


	void cadenas(){

		std::string str = "jose";
		std::string str2 = " manuel";
		std::string str3 = str + str2;
		std::cout << str3 << std::endl; 

		// char = 1byte = 0000 0000


		// cadena con tamaño  fijo,  se puede cambiar los valores de cada char
		char saludo[] = "Hola Mundo";
		//  '\0'  significa caracter nulo
		char saludo2[] = {'H','o','l','a',' ','M','u','n','d','o','\0'};

		// puntero a char
		char* str4 = &saludo2[0];

	}

	void punteros(){
		int i = 8888;
		int* p = &i;

		// el puntero tambien tiene direccion, se coloca su direccion
		int** p2 = &p;

		std::cout << "dir. original: " << &i << std::endl; 
		// obtener la dir que almacena el puntero
		std::cout << "dir. a original: " << p << std::endl; 
		// obtener la dir del puntero
		std::cout << "dir. puntero: " << &p << std::endl; 
		std::cout << "dir. puntero doble: " <<  p2 << std::endl; 
		std::cout << "val. puntero doble: " << **p2 << std::endl; 

	}

	void referencias(){
		// referencias no pueder nulas
		// solo pueden ser reasignadas
		// intentar reasignarlas cambiaria el valor de la direccion a la que hace referencia

		int a = 5;
		int b = 8;
		// no se usa el &a para asignar la direcion a la referencia
		int& ref = a;
		// solo cambiaria el valor de 'a' a 8
		ref = b;


	}

	void Gato::Maullar(){
		
	}
	
}
