#include <iostream>

#include "libs/dinamicas/biblioteca.h"
// #include "perro.h"

#define PI 2000

// extern va donde se va usar
extern "C" {
  #include "funciones.h" //a C header, so wrap it in extern "C" 
}



// tdos es publico por defecto y se asignan al stack
struct Punto { 
   int x, y; 
   Punto() {x = 20; y = 20;} // Constructor

   void Multiplicar() {  
   	std::cout << "PuntosMultiplicados: " << x*y << std::endl;
   }

} Punto1, Punto2;




class Humano
{
	private:
		unsigned int edad;
	public:
		Humano(){
			this->edad = 20;
		};
		~Humano(){
			std::cout << "eliminado correctamente" << std::endl;

		};


		float altura = 1.70;

		void mostrarEdad(){
			std::cout << "edad: " << this->edad << std::endl;

		};
};

int main(){

	std::cout << "hola" << std::endl;
	std::cout << PI << std::endl;


	for(int i = 1; i <= 5; i++){
		std::cout << i << std::endl;
	}

	int n = jseh::func(50,50);

	std::cout << "el numero es: " << n << std::endl;
	

	// creado en el heap
	// Humano* h = new Humano();
	auto h = new Humano();
	// creado en el stack
	// Humano hh = Humano();
	auto hh = Humano();
	// si esta en el stack se usa notacion de .
	hh.mostrarEdad();

	// si esta en el heap se usa notacion de ->
	h->mostrarEdad();
	std::cout << "altura: " << h->altura << std::endl;

	delete h;

	// entero asignado en el heap
	int* p = new int(5555);
	// obtener valor
   	std::cout << *p << std::endl;
   	//obtener direccion
   	std::cout << p << std::endl;
   	delete p;


   
   	std::cout << Punto1.x << std::endl;

   	Punto1.Multiplicar();

   	// checar si ya no se puede asignar mas memoria al heap
   	int* puntero = new(std::nothrow) int;
	if (!puntero)
	{
	   std::cout << "Memory allocation failed\n";
	}else{
		*puntero = 2222; 
        std::cout << "valor de puntero: " << *puntero << std::endl; 
	}


	delete puntero;

	puntero = NULL;
	delete puntero;

    int* pa = new int[10];
    delete[] pa;


    // no se puede liberar la memoria dos veces con delete, si ya ha sido liberada, reasignarlo a NULL o a direccion valida
    
    // todos lo apuntadores deben ser inicializados con una direccion valida o con NULL

    // no reasignar puntero ( int* p = new int, *p = 20, p = new int(40) ), sin antes haber aplicado delete, porque la memoria se pierde

    // no acceder al valor de un puntero ya eliminado con delete (delete p; *p = 20; ), sin reasignarlo primero con direccion valida 


    // Perro* mc = new Perro();
    // jseh::Gato* g = new jseh::Gato();
    // delete mc;


    


    jseh::cadenas();
    jseh::arrays();
    jseh::punteros();


    //invocar funcion de c
   	std::cout << "sumar de c: " << sumar(60, 60) << std::endl;

	return 0;


}



