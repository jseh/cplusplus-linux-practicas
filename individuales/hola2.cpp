#include <cmath>
#include <iostream>
#include <functional>
#include <vector>
#include <array>
#include <tuple> 
#include <string> 

using i8  = int8_t;
using i16 = int16_t;
using i32 = int32_t;
using i64 = int64_t;
using u8  = uint8_t;
using u16 = uint16_t;
using u32 = uint32_t;
using u64 = uint64_t;


#define PRINT(x) std::cout << x << endl 

using std::cout;
using std::endl;
using std::cin;

template<typename Z>
class clase_generica{
	Z nombre;
	
};


template<typename T, typename X>
void fn_generica(T a, X b) {

}

template<typename N>
void fn_generica2(N* a) {

}

class ClaseA {
	private:
		int pa;
		int pb; 
		// indicamos que Clase2 puede acceder a miembros prrivados de esta clase
		friend class ClaseB; 
		// es posible agregar una funcion que puede ser un metodo a otra clase o una funcion global
		friend void modificarPrivado(ClaseA&);
		
	public:
		// usando el inicializador de lista
		ClaseA(int a = 20, int b = 40): pa(a), pb(b) {};
};


class ClaseB {
	private:
		int pc;
		int pd;
	public: 
		int pf;

	    void showPA(ClaseA& x) { 
	        std::cout << "ClaseA::pa=" << x.pa; 
	    } 

	    // con const no puede cambiar el estado  de la clase, es decir las variables
	    void cambiarValor() const {
	    	// pf = 2; // no permitido

	    }
};

void modificarPrivado(ClaseA& x) {
	std::cout << "ClaseA::pa=" << x.pa; 
}

int main() {
	PRINT("ok");
	auto cg = new clase_generica<double>();
	delete cg;
	fn_generica<int,int>(5,10);

	auto ca = new ClaseA(4, 5);
	delete ca;

	// modificadores de tipos basicos
	// signed - integer, char, long-prefix
	// unsigned - integer, char, short-prefix
	// long - integer, double
	// short - integer

	i8 ie = 2;

	int i = 4;
	short int si = 1;
	unsigned short int usi = 1;
	long int li = 1;
	long double ld = 123123.034;
	long long int lli = 23423423423;
	double d = 2.5;
	float f = 5.3f;
	char c = 'k';
	wchar_t wc = 'w';


	// long a;  --> por defecto representa un long int.



	// widening tipo pequeño a grande
	// narrowing tipo grande a pequeño


	// redondeo hacia abajo
	int n1;
	float f1 = 3.142f;
	n1 = static_cast<int>(f1);

	PRINT(n1);

	float f2;
	int n2 = -10;
	f2 = static_cast<float>(n2);
	PRINT(n2);


	


	// reinterpret_cast
	// It is used to convert one pointer of another pointer of any type, no matter either the class is related to each other or not.
	// It does not check if the pointer type and data pointed by the pointer is same or not.
	// Se usa para hacer cambios de tipo a nivel de bits, es decir, el valor de la "expresión" se interpreta como si fuese un objeto del tipo "tipo"
	// no puede castear const, volatile, or __unaligned
	// Este tipo de conversión es peligrosa, desde el punto de vista de la compatibilidad, hay que usarla con cuidado.

	int n3 = 1051;
	int* p3;
	p3 = reinterpret_cast<int*>(n3);
	PRINT(p3);
	// no puedo borrar p3, porque es una referencia
	// delete p3;




	// dynamic_cast
	// es usado promariamante para hace downcast seguros, clase base a derivada
	// funciona en clases con herencia, este operador sólo puede usarse con objetos polimórficos(clase base deberia contener al menos un metodo virtual), cualquier intento de aplicarlo a objetos de tipos fundamentales o agregados o de clases no polimórficas dará como resultado un error.
	// regresa NULL  si el casteo falla
	// hay dos modalidades de dynamic_cast, una usa punteros y la otra referencias:
	// Modalidad de puntero:
	// Derivada *p = dynamic_cast<Derivada *> (&Base);
	// Modalidad de referencia:
	// Derivada &refd = dynamic_cast<Derivada &> (Base);
	// da error si se intenta convertir un puntero Base*
	// java idiom
	// void foo(From asFrom) {
	//      if (asFrom instanceof To) {
	//          To asTo = (To)asFrom;
	//          // do something
	//      }
	//  }




	// const_cast
	// Sin embargo, este operador se usa para obtener expresiones donde se necesite añadir o eliminar esos modificadores. 
	// Si se intenta modificar el valor de una expresión constante, el resultado es indeterminado
	const int* p4 = new int(5);
	// me devuelve un puntero a a la misma locacion
	int* n4 = const_cast<int*>(p4);	
	PRINT(*n4);
	*n4 = 2;
	PRINT(*n4);
	// *p4 = 6; // error no se puede reasignar el valor, es de solo lectura
	PRINT(*p4);





	return 0;
}