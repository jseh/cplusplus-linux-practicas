#include <cmath>
#include <iostream>
#include <functional>
#include <vector>
#include <array>
#include <tuple> 
#include <string> 
#include <chrono>
#include <thread>
#include <future>

#define log(x) std::cout << x << endl 

using std::cout;
using std::endl;
using std::cin;

using namespace std::chrono;

std::string fetchDataFromDB(std::string recvdData){
	std::this_thread::sleep_for(seconds(5));
	return "DB_" + recvdData;
}
 
std::string fetchDataFromFile(std::string recvdData){
	std::this_thread::sleep_for(seconds(5)); 
	log("listo");

	return "File_" + recvdData;
}
 

int main() {

	system_clock::time_point start = system_clock::now();

	// std::launch::deferred ejecuta la funcion cuando se llama a get()
	// std::launch::async ejecuta la funcion inmediatamente
	std::future<std::string> resultFromDB = std::async(std::launch::async, fetchDataFromDB, "Data");

	std::string fileData = fetchDataFromFile("Data");

	std::string dbData = resultFromDB.get();
	



	// al final terminan las dos en 5 sec
	auto end = system_clock::now();
	auto diff = duration_cast < std::chrono::seconds > (end - start).count();
	std::cout << "Total Time Taken = " << diff << " Seconds" << std::endl;
	//Combine The Data
	std::string data = dbData + " :: " + fileData;
	//Printing the combined Data
	std::cout << "Data = " << data << std::endl;
	



	return 0;
}