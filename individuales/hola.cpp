#include <cmath>
#include <iostream>
#include <functional>
#include <vector>
#include <array>
#include<tuple> 
#include<string> 

#define  PRINT(x) std::cout << x << endl 

//ok ok
// using namespace std;
using std::cout;
using std::endl;
using std::cin;


// no usar herencia con estructuras
// por lo general se pasan por copia
// visiblidad es por defecto publico
struct Estructura{
	int a;
	float b;
};



void f(int a, int b) {
	cout << (a*b) << endl;
}

//recibe una funcion
void f2(int a, void fn(int, int) ) {
	fn(a,5);
}

// recibe lambda
void f3(std::function<void(int)> lambda){
	lambda(50);
}

// array son pasados por referencias
void f4(int ar[3]){
	ar[1] = 99999;
}

// recibe array por valor
void f5(std::array<int, 2> ar){
	cout << ar[0] << endl;
	ar[0] = 21;
	cout << ar[0] << endl;
}

// recibe una referencia
void f6(int& n){
	n = 8888;
}

void copiarArray(){
	int X[8] = {0,1,2,3,4,5,6,7};
    std::array<int,8> Y;
    std::copy(std::begin(X), std::end(X), std::begin(Y));
    for (int i: Y)
        std::cout << i << " ";
    std::cout << '\n';
}

void recibirEstructura(Estructura e){
	e.a = 222222;
	PRINT(e.a);
}

void smartPointers(){

}

void print(const std::string &s)
{
	for (char const &c: s) {
		std::cout << c << ' ';
	}

	std::cout << endl;
}


void contenedores(){
	std::array<int, 5> n = {1, 2, 3, 4, 5};

	// multidimensional
	std::array<std::array<int, 3>, 3> a2d { {{1,2,3}, {4,5,6}, {7,8,9}} };

	//crea uno similar a a un array con tamaño fijo
	std::vector<int> arr(5);

	std::vector<int> v = {2,4,5,1,6};
	// f.push_back(20);

	for (int x : v) 
	   cout << x << endl; 
}

int main() {

	f(2, 2);
	f2(2, f );
	f3([](int x){ cout << x * 2 << endl; } );
	contenedores();

	// std::array esa pasado por valor
	std::array<int, 2> n = {1, 2};
	f5(n);
	cout << n[0] << endl;

	// c-array es pasado por ref por defecto
	int a[3] = {2,5,7};
	cout << "a[1] antes: " << a[1] << endl;
	f4(a);
	cout << "a[1] despues: " << a[1] << endl;

	// mkj
	int ref = 30;
	cout << "ref antes: " << ref << endl;
	f6(ref);
	cout << "ref despues: " << ref << endl;


	std::tuple <char, int, float> geek; 
    geek = std::make_tuple('a', 10, 15.5); 

    cout << "The size of tuple is : "; 
    cout << std::tuple_size<decltype(geek)>::value << endl; 

    cout << "geek<0>: " << std::get<0>(geek) << endl;

    std::tuple <char, int, float, int> geek2; 
    geek2 = std::make_tuple('a', 10, 15.5, 20); 

    copiarArray();

    print("Jose Manuel");

    std::string myString = "Hello World";
    // std::wstring myString2 = "Hello World-unicode";


    Estructura s; 
	cout << "Estructura-a: " << (s.a = 55555) << endl;
	recibirEstructura(s);
	cout << "Estructura-a: " << (s.a = 55555) << endl;



  return 0;
}