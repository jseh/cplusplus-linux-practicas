#include <cmath>
#include <iostream>
#include <functional>
#include <vector>
#include <array>
#include <tuple> 
#include <string> 




#define PRINT(x) std::cout << x << endl 

using std::cout;
using std::endl;
using std::cin;

class Oso{

	public:
		// se agrega virtual a los metodos que pueden ser sobreescribidos por las clases derivadas
		virtual void comer(){
			std::string n = "OSO Comiendo...";
			PRINT(n);
		}

		void llamar_comer(){
			this->comer();
		}
};

class OsoPolar : public Oso{

	public:
		// override esta para no olvidar marcar como virtual, los metodos padres que se van a sobreescribir
		void comer() override {
			std::string n = "OSO Polar Comiendo...";
			PRINT(n);
		}
};

class OsoPardo : public Oso{
	public:
		void comer() override {
			std::string n = "OSO Pardo Comiendo...";
			PRINT(n);
			Oso::comer();
		}
};

void manejarOso(Oso* o){
	o->comer();

	delete o;
}






class IBox {
   public:
      // pure virtual function
      virtual double getVolume() = 0;
      
   private:
      double length;      // Length of a box
      double breadth;     // Breadth of a box
      double height;      // Height of a box
};


class CajaGalletas: public IBox{

};

class CajaCereal: public IBox{
	public: 
		double getVolume() override {
			return 2.0;
		}
};


void manejarCaja(IBox* c){
	delete c;
}



int main(){
	// auto o = new Oso();
	// o->comer();

	// OsoPolar* op = new OsoPolar();
	// op->comer();


	// Oso* op2 = new OsoPolar();
	// op2->comer();

	// manejarOso(new Oso());
	// manejarOso(new OsoPolar());
	manejarOso(new OsoPardo());

	manejarCaja(new CajaCereal());

	// delete o;
	// delete op;
	// delete op2;
	return 0;
}